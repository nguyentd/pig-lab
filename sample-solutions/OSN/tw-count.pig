	dataset = LOAD 'sample-input/OSN/tw.txt' AS (id: long, fr: long);

-- TODO: check if user IDs are valid (e.g. not null) and clean the dataset
	good_data = FILTER dataset BY id is not null and fr is not null; 
	bad_data = FILTER dataset BY id is null and fr is null;

-- TODO: organize data such that each node ID is associated to a list of neighbors
	nodes = GROUP good_data BY id; 

-- TODO: foreach node ID generate an output relation consisting of the node ID and the number of "friends"
	friends = FOREACH nodes GENERATE group, COUNT(good_data) AS followers;

-- count the following
	nodes2 = GROUP good_data BY fr;
	followings = FOREACH nodes2 GENERATE group, COUNT(good_data);

-- find the outliers
	outliers = FILTER friends BY followers<2 ;

	STORE friends INTO './sample-output/OSN/twc/';
	STORE followings INTO './sample-output/OSN/following/';
	STORE outliers INTO './sample-output/OSN/outliers/';
