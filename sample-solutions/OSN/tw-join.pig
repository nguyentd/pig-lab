set job.name 'Twitterjoin_OSN_EX1_GROUP08';

-- TODO: load the input dataset, located in ./local-input/OSN/tw.txt
A = LOAD '/laboratory/twitter-big.txt' AS (id: long, fr: long);
B = A;

good_dataA = FILTER A BY id is not null and fr is not null;
good_dataB = FILTER B BY id is not null and fr is not null;
DUMP good_dataA;

-- TODO: compute all the two-hop paths 
twohop = JOIN good_dataA by $1, good_dataB by $0;  

-- TODO: project the twohop relation such that in output you display only the start and end nodes of the two hop path
p_result = FOREACH twohop GENERATE $0, $3;
result = DISTINCT p_result;


-- TODO: make sure you avoid loops (e.g., if user 12 and 13 follow eachother) 
result = FILTER result by id != fr;
DUMP result;

STORE result INTO 'user/group08/sample-output/OSN/tw-join/';
