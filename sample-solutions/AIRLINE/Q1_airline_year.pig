SET default_parallel 10;
set job.name 'AIRLINE_EX1_YEAR_GROUP08';

%declare INPUT_PATH '/laboratory/airlines/2008.csv';
%declare OUTPUT_PATH '/user/group08/sample-output/AIRLINE/EX1/';

-- Load raw data
RAW_DATA = LOAD '$INPUT_PATH' USING PigStorage(',') AS 
	(year: int, month: int, day: int, dow: int, 
	dtime: int, sdtime: int, arrtime: int, satime: int, 
	carrier: chararray, fn: int, tn: chararray, 
	etime: int, setime: int, airtime: int, 
	adelay: int, ddelay: int, 
	scode: chararray, dcode: chararray, dist: int, 
	tintime: int, touttime: int, 
	cancel: chararray, cancelcode: chararray, diverted: int, 
	cdelay: int, wdelay: int, ndelay: int, sdelay: int, latedelay: int);

-- Aggregate in-bound traffic
INBOUND = FOREACH RAW_DATA GENERATE year AS y, dcode AS d;
GROUP_INBOUND = GROUP INBOUND BY (y,d);
COUNT_INBOUND = FOREACH GROUP_INBOUND GENERATE FLATTEN(group), COUNT(INBOUND) AS count;
GROUP_COUNT_INBOUND = GROUP COUNT_INBOUND BY y;
topYearlyInbound = FOREACH GROUP_COUNT_INBOUND {
    result = TOP(20, 0, COUNT_INBOUND); 
    GENERATE FLATTEN(result);
}
STORE topYearlyInbound INTO '$OUTPUT_PATH/INBOUND-TOP/YEAR' USING PigStorage(',');

-- Aggregate out-bound traffic
OUTBOUND = FOREACH RAW_DATA GENERATE year AS y, scode AS s;
GROUP_OUTBOUND = GROUP OUTBOUND BY (y,s);
COUNT_OUTBOUND = FOREACH GROUP_OUTBOUND GENERATE FLATTEN(group), COUNT(OUTBOUND) AS count;
GROUP_COUNT_OUTBOUND = GROUP COUNT_OUTBOUND BY y;
topYearlyOutbound = FOREACH GROUP_COUNT_OUTBOUND {
    result = TOP(20, 0, COUNT_OUTBOUND); 
    GENERATE FLATTEN(result);
}
STORE topYearlyOutbound INTO '$OUTPUT_PATH/OUTBOUND-TOP/YEAR' USING PigStorage(',');

-- Aggregate traffic
UNION_TRAFFIC = UNION COUNT_INBOUND, COUNT_OUTBOUND;
GROUP_UNION_TRAFFIC = GROUP UNION_TRAFFIC BY (y,d);
TOTAL_TRAFFIC = FOREACH GROUP_UNION_TRAFFIC GENERATE FLATTEN(group) AS (y,code), SUM(UNION_TRAFFIC.count) AS total; 
TOTAL_YEARLY = GROUP TOTAL_TRAFFIC BY y;

topYearlyTraffic = FOREACH TOTAL_YEARLY {
    result = TOP(20, 0, TOTAL_TRAFFIC); 
    GENERATE FLATTEN(result) AS (year, iata, traffic);
}


STORE topYearlyTraffic INTO '$OUTPUT_PATH/MONTHLY-TRAFFIC-TOP/YEAR' USING PigStorage(',');









